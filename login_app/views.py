from django.shortcuts import render, reverse
from login_app.models import CustomUser
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout
from django.http import HttpResponseRedirect


def index(request):
    pass


def login(request):
    context = {}
    if request.method == "POST":
        user = authenticate(
            request, username=request.POST['username'],  password=request.POST['password'], )
        if user:
            dj_login(request, user)
            return HttpResponseRedirect(reverse('banking_app:index'))
        else:
            context = {
                'error': 'Bad username or password.'
            }
    return render(request, 'login_app/login.html', context)


def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse('login_app:login'))


def sign_up(request):
    context = {}
    if request.method == "POST":
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        if password == confirm_password:
            if CustomUser.objects.create_user(
                    username=request.POST['username'],
                    first_name=request.POST['first_name'],
                    last_name=request.POST['last_name'],
                    phone=request.POST['phone'],
                    email=request.POST['email'],
                    password=request.POST['password']):
                return HttpResponseRedirect(reverse('login_app:login'))
            else:
                context = {
                    'error': 'Could not create user account - please try again.'
                }
        else:
            context = {
                'error': 'Passwords did not match. Please try again.'
            }
    return render(request, 'login_app/sign_up.html', context)
