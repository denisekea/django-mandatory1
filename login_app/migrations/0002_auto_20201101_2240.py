# Generated by Django 3.1.2 on 2020-11-01 21:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('login_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='rank',
            field=models.CharField(choices=[('basic', 'basic'), ('silver', 'silver'), ('gold', 'gold')], default='basic', max_length=30),
        ),
    ]
