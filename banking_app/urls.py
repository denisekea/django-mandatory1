from django.urls import path
from . import views

app_name = 'banking_app'

urlpatterns = [
   path('', views.index, name='index'),
   path('transfer', views.transfer, name='transfer'),
   path('loanpayment', views.loan_payment, name='loanpayment'),
   path('bankaccount/<int:accountno>', views.bankaccount_details, name='bankaccount'),
   path('loan/<int:loanno>', views.loan_details, name='loan'),
   path('profile', views.profile, name='profile'),
]