# django-mandatory1

## ER Diagram

[See ERD on LucidChart](https://app.lucidchart.com/lucidchart/5dcd34b2-350a-41cf-894a-7e1d7f9002f1/view)

## Start virtual environment

After you started the venv, install project's pip packages in the project directory:

`pip install -r requirements.txt`

## Migrate project

`python manage.py makemigrations`

`python manage.py migrate`

## Run project

`python manage.py runserver`


## Login to banking app

Go to: `localhost:8000`

username: henrik <br />
password: kea12345

Basic user:

username: denny <br />
password: kea12345

## Login as bank employee / admin

Go to: `localhost:8000/admin`

username: henrik <br />
password: kea12345